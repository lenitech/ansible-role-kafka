auto_auth {
  method "approle" {
    config = {
      role_id_file_path = "/etc/vault.d/role-id"
      secret_id_file_path = "/etc/vault.d/secret-id"
      remove_secret_id_file_after_reading = false
    }
  }
}

template_config {
  exit_on_retry_failure = true
  static_secret_render_interval = "5m"
}

template {
  source      = "/etc/vault.d/cert.ctmpl"
  destination = "/etc/ssl/certs/kafka.pem"
  perms       = "0644"
  error_on_missing_key = true
  exec {
    command = ["openssl pkcs12 -export -passout pass:changeit -inkey /etc/ssl/private/kafka.key -in /etc/ssl/certs/kafka.pem -certfile /etc/ssl/certs/ca.pem -out /etc/ssl/certs/java/kafka.jks && chmod 640 /etc/ssl/certs/java/kafka.jks && chgrp ssl-cert /etc/ssl/certs/java/kafka.jks && keytool -importcert -noprompt -trustcacerts -file /etc/ssl/certs/ca.pem -keystore /etc/ssl/certs/java/kafka-ca.jks -storepass changeit && systemctl restart kafka"]
  }
}
