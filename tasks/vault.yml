---
- name: add Hashicorp apt repository key
  get_url:
    url: https://apt.releases.hashicorp.com/gpg
    dest: /etc/apt/trusted.gpg.d/hashicorp-keyring.asc
    mode: "0644"
    owner: root
    group: root
    force: true
  tags:
    - vault

- name: add Hashicorp apt repository
  apt_repository:
    repo: "deb [signed-by=/etc/apt/trusted.gpg.d/hashicorp-keyring.asc] https://apt.releases.hashicorp.com {{ ansible_distribution_release }} main"
    filename: hashicorp
    state: present
  tags:
    - vault

- name: ensure vault is installed
  apt:
    name:
      - vault
    policy_rc_d: 101
    state: present
  tags:
    - vault

- name: disable vault server service
  systemd:
    name: vault
    state: stopped
    enabled: False
  tags:
    - vault

- name: create vault-agent systemd unit
  copy:
    src: vault-agent.service
    dest: /etc/systemd/system/vault-agent.service
    mode: "0755"
    owner: root
    group: root
  tags:
    - vault

- name: copy vault templates
  copy:
    src: "{{ item }}"
    dest: "/etc/vault.d/{{ item }}"
    mode: "0644"
    owner: vault
    group: vault
  loop:
    - cert.ctmpl
  tags:
    - vault

- name: copy vault HCL config
  copy:
    src: vault.hcl
    dest: /etc/vault.d/vault.hcl
    mode: "0644"
    owner: vault
    group: vault
  tags:
    - vault

- name: copy vault ENV config
  template:
    src: vault.env
    dest: /etc/vault.d/vault.env
    mode: "0644"
    owner: vault
    group: vault
  tags:
    - vault

- name: copy vault approle role-id
  copy:
    content: "{{ kafka_vault_role_id }}"
    dest: /etc/vault.d/role-id
    mode: "0644"
    owner: root
    group: root
  tags:
    - vault
    - approle

- name: copy vault approle secret-id
  copy:
    content: "{{ kafka_vault_secret_id }}"
    dest: /etc/vault.d/secret-id
    mode: "0640"
    owner: root
    group: root
  no_log: True
  tags:
    - vault
    - approle

- name: start and enable vault service
  systemd:
    name: vault-agent
    state: restarted
    enabled: True
    daemon_reload: True
  tags:
    - vault
